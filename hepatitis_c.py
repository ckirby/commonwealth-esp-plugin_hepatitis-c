'''
                                  ESP Health
                                 Hepatitis C
                              Disease Definition


@author: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@contact: http://www.esphealth.org
@copyright: (c) 2011-2012 Channing Laboratory
@license: LGPL
'''

# In most instances it is preferable to use relativedelta for date math.
# However when date math must be included inside an ORM query, and thus will
# be converted into SQL, only timedelta is supported.
#
# This may not still be true in newer versions of Django - JM 6 Dec 2011
from collections import defaultdict
from decimal import Decimal

from dateutil.relativedelta import relativedelta
from django.db import IntegrityError

from ESP.hef.base import DiagnosisHeuristic
from ESP.hef.base import Dx_CodeQuery
from ESP.hef.base import LabResultFixedThresholdHeuristic
from ESP.hef.base import LabResultPositiveHeuristic, LabResultAnyHeuristic
from ESP.hef.models import Event
from ESP.nodis.base import DiseaseDefinition
from ESP.utils import log


class PotentialCase(object):
    def __init__(self, date, criteria, event, events):
        self.date = date
        self.criteria = criteria
        self.event = event
        self.events = events


class PotentialCaseList(object):
    def __init__(self, patient):
        self.patient = patient
        self.cases = []

    def add_case(self, case):
        self.cases.append(case)

    def get_earliest_case(self):
        self.cases.sort(key=lambda phc: phc.date)
        return self.cases[0]

    def gather_events(self):
        ev = []
        for case in self.cases:
            ev.extend(case.events)
        return set(ev)


class Hepatitis_C(DiseaseDefinition):
    '''
    Hepatitis C
    '''
    # A future version of this disease definition may also detect chronic hep c
    conditions = ['hepatitis_c:acute']

    uri = 'urn:x-esphealth:disease:channing:hepatitis-combined:hepatitis_c:v1'

    short_name = 'hepatitis_c:acute'

    test_name_search_strings = [
        'hep',
        'alt',
        'ast',
        'bili',
        'tbil',
        'hbv',
        'hcv',
        'sgpt',
        'sgot',
        'aminotrans',
        'genotype'
    ]

    potential_case_klass = PotentialCase
    potential_case_list_klass = PotentialCaseList
    
    timespan_heuristics = []

    def __init__(self, *args, **kwargs):
        super(Hepatitis_C, self).__init__(*args, **kwargs)
        self.potential_cases = {}

    @property
    def event_heuristics(self):
        '''
        Event heuristics used by all Hepatitis variants
        '''
        heuristic_list = []
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_c_elisa',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_c_signal_cutoff',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_c_riba',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_c_rna',
        ))
        heuristic_list.append(LabResultAnyHeuristic(
            test_name='hepatitis_c_genotype',
        ))
        heuristic_list.append(DiagnosisHeuristic(
            name='hepatitis_c:chronic',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='B18.2', type='icd10'),
                Dx_CodeQuery(starts_with='070.54', type='icd9'),
            ]
        ))
        heuristic_list.append(DiagnosisHeuristic(
            name='hepatitis_c:unspecified',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='B19.20', type='icd10'),
                Dx_CodeQuery(starts_with='070.70', type='icd9'),
            ]
        ))
        heuristic_list.append(DiagnosisHeuristic(
            name='jaundice',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='R17', type='icd10'),
                Dx_CodeQuery(starts_with='782.4', type='icd9'),
            ]
        ))
        heuristic_list.append(LabResultFixedThresholdHeuristic(
            test_name='alt',
            threshold=Decimal('200'),
            match_type='gt',
        ))
        heuristic_list.append(LabResultFixedThresholdHeuristic(
            test_name='bilirubin_total',
            threshold=Decimal('1.5'),
            match_type='gt',
        ))

        return heuristic_list

    def generate_hepc(self):
        self._condition_a()
        self._condition_b()
        self._condition_c()
        self._condition_d()

        return self._process_potential_cases()

    def generate(self):
        log.info('Generating cases for %s (%s)' % (self.short_name, self.uri))
        counter = self.generate_hepc()
        log.info('Generated %s new cases for %s (%s)' % (counter, self.short_name, self.uri))
        return counter

    def _condition_ab(self, required_names, negating_names, validating_names, invalidating_names, criteria):
        required_events = self._get_event_values(required_names)
        patient_events = self._build_patient_event_dict(required_events)
        self._update_event_dict_for_existing_cases(patient_events)
        self._remove_negated_events(negating_names, patient_events, validating_names, 28)

        for patient, events in patient_events.items():
            potential_validating = Event.objects.filter(patient=patient, name__in=validating_names)
            potential_negating = Event.objects.filter(patient=patient, name__in=invalidating_names)
            for event in events:
                start = event['date'] - relativedelta(days=28)
                end = event['date'] + relativedelta(days=28)
                validating_events = list(
                    potential_validating.filter(date__gte=start, date__lte=end).values_list('pk', flat=True))
                has_negating_events = potential_negating.filter(date__gte=start, date__lte=end).count() > 0

                if len(validating_events) > 0 and not has_negating_events:
                    self.populate_potential_cases(patient, self.potential_case_klass(event['date'], criteria, event,
                                                                         validating_events + [e['pk'] for e in events]))
                    continue

    def _condition_cd(self, required_names, negating_names, validating_names, criteria):
        required_events = self._get_event_values(required_names)
        patient_events = self._build_patient_event_dict(required_events)
        self._update_event_dict_for_existing_cases(patient_events)
        self._remove_negated_events(negating_names, patient_events)

        for patient, events in patient_events.items():
            potential_validating = Event.objects.filter(patient=patient, name__in=validating_names)
            for event in events:
                start = event['date'] - relativedelta(years=1)
                end = event['date']
                validating_events = list(
                    potential_validating.filter(date__gte=start, date__lt=end).values_list('pk', flat=True))

                if len(validating_events) > 0:
                    self.populate_potential_cases(patient, self.potential_case_klass(event['date'], criteria, event,
                                                                         validating_events + [e['pk'] for e in events]))
                    continue

    def _condition_a(self):
        required_names = ['dx:jaundice',
                          'lx:alt:threshold:gt:200',
                          'lx:bilirubin_total:threshold:gt:1.5']

        negating_names = ['lx:hepatitis_c_riba:positive',
                          'lx:hepatitis_c_rna:positive',
                          'lx:hepatitis_c_elisa:positive',
                          'dx:hepatitis_c:chronic',
                          'dx:hepatitis_c:unspecified']

        validating_names = ['lx:hepatitis_c_elisa:positive']

        invalidating_names = ['lx:hepatitis_c_riba:negative',
                              'lx:hepatitis_c_riba:indeterminate',
                              'lx:hepatitis_c_rna:negative',
                              'lx:hepatitis_c_rna:indeterminate',
                              'lx:hepatitis_c_signal_cutoff:negative',
                              'lx:hepatitis_c_signal_cutoff:indeterminate']

        self._condition_ab(required_names, negating_names, validating_names, invalidating_names, 'Criteria a')

    def _condition_b(self):
        required_names = ['dx:jaundice',
                          'lx:alt:threshold:gt:200',
                          'lx:bilirubin_total:threshold:gt:1.5']

        negating_names = ['lx:hepatitis_c_riba:positive',
                          'lx:hepatitis_c_rna:positive',
                          'lx:hepatitis_c_elisa:positive',
                          'dx:hepatitis_c:chronic',
                          'dx:hepatitis_c:unspecified']

        validating_names = ['lx:hepatitis_c_rna:positive']

        invalidating_names = ['lx:hepatitis_c_riba:negative',
                              'lx:hepatitis_c_riba:indeterminate',
                              'lx:hepatitis_c_signal_cutoff:negative',
                              'lx:hepatitis_c_signal_cutoff:indeterminate']

        self._condition_ab(required_names, negating_names, validating_names, invalidating_names, 'Criteria b')

    def _condition_c(self):
        required_names = ['lx:hepatitis_c_rna:positive']

        negating_names = ['lx:hepatitis_c_riba:positive',
                          'lx:hepatitis_c_rna:positive',
                          'lx:hepatitis_c_elisa:positive',
                          'dx:hepatitis_c:chronic',
                          'dx:hepatitis_c:unspecified']

        validating_names = ['lx:hepatitis_c_rna:negative',
                            'lx:hepatitis_c_elisa:negative']

        self._condition_cd(required_names, negating_names, validating_names, 'Criteria c')

    def _condition_d(self):
        required_names = ['lx:hepatitis_c_elisa:positive']

        negating_names = ['lx:hepatitis_c_riba:positive',
                          'lx:hepatitis_c_rna:positive',
                          'lx:hepatitis_c_elisa:positive',
                          'dx:hepatitis_c:chronic',
                          'dx:hepatitis_c:unspecified']

        validating_names = ['lx:hepatitis_c_rna:negative',
                            'lx:hepatitis_c_elisa:negative']

        self._condition_cd(required_names, negating_names, validating_names, 'Criteria d')

    def _remove_negated_events(self, negating_names, patient_events, offset_names=None, offset=0):
        negating_events = Event.objects.filter(
            name__in=negating_names
        ).order_by('date').values('name', 'patient', 'date')
        patient_negating_events = defaultdict(list)
        if offset_names is None:
            offset_names = []
        for ne in negating_events:
            date = ne['date']
            if ne['name'] in offset_names:
                date = date + relativedelta(days=offset)
            patient_negating_events[ne['patient']].append(date)
        """
        Remove trigger events that are after negating events. If a patient has no more triggering events the remove them
        from the patient_events dict.
        """
        for patient, negate_dates in patient_negating_events.items():
            negate_dates.sort()
            events = patient_events.get(patient, None)
            if events is None:
                continue
            potentials = []
            for event in events:
                if event['date'] <= negate_dates[0]:
                    potentials.append(event)

            if potentials:
                patient_events[patient] = potentials
            else:
                patient_events.pop(patient, None)

    def _get_event_values(self, required_names):
        return Event.objects.filter(
            name__in=required_names
        ).exclude(case__condition=self.conditions[0]).order_by('date').values('pk', 'name', 'patient', 'date')

    def _build_patient_event_dict(self, events):
        patient_events = defaultdict(list)
        for ev in events:
            patient_events[ev['patient']].append(ev)

        return patient_events

    def _update_event_dict_for_existing_cases(self, events):
        patients_with_existing_cases = self.add_events_to_existing_cases(events.keys(),
                                                                         [self._extract_pks(events)])
        pt_ids_for_existing_cases = set([p.pk for p in patients_with_existing_cases])

        for pid in pt_ids_for_existing_cases:
            events.pop(pid, None)

    def _extract_pks(self, patient_event_dict):
        return {patient: [event['pk'] for event in events] for patient, events in patient_event_dict.items()}

    def populate_potential_cases(self, patient, potential_case):
        self.potential_cases[patient] = self.potential_cases.get(patient, self.potential_case_list_klass(patient))
        self.potential_cases[patient].add_case(potential_case)

    def _process_potential_cases(self):
        new_cases = 0
        for patient, potential_list in self.potential_cases.items():
            first_case = potential_list.get_earliest_case()
            try:
                log.debug('Good Case for %s' % patient)
                t, new_case = self._create_case_from_event_list(
                    condition=self.conditions[0],
                    criteria=first_case.criteria,
                    recurrence_interval=None,  # Does not recur
                    event_obj=Event.objects.get(pk=first_case.event['pk']),
                    relevant_event_names=potential_list.gather_events() - {first_case.event['pk']},
                )
            except IntegrityError:
                log.debug('Case creation failed for     %s' % patient)
                pass  # _create_case_from_event_list only knows how to create a case and return True.
            if t:
                new_cases += 1
                log.info('Created new hepc case %s : %s' % (first_case.criteria, new_case))

        return new_cases


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Packaging
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def event_heuristics():
    return Hepatitis_C().event_heuristics


def disease_definitions():
    return [Hepatitis_C()]
